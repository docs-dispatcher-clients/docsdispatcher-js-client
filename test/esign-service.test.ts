import ESignDispatcher, { ESignDispatcherPayload, RecipientRole } from '../src/dispatcher/ESignDispatcher'
import UploadDispatcher from '../src/dispatcher/UploadDispatcher'
import { Attachment } from '../src/dispatcher/interfaces/attachments'
import { BasicRequest } from '../src/dispatcher/interfaces/basic-requests'
import { Target, TargetTypes } from '../src/dispatcher/interfaces/generics'
import RequestDescriptor from '../src/http/interfaces/RequestDescriptor'
import makeLiveDD from './live-tests/dd-live'
import makeDDStub from './mocks/dd-http-stub'

function fixtureESignDispatcher(): ESignDispatcher {
  return new ESignDispatcher()
    .withProvider('SIGNATURIT')
    .addDocument(new Attachment({
      url: 'https://site.com/file.pdf',
    }))
    .addDocument(new Attachment({
      content: 'dlkfzdlgfzed',
    }))
    .addDocument(new Attachment({
      templateName: 'superadmincompany-admin-test-small-docx-1',
      data: {
        name: 'Steve',
      },
    }))
    .addRecipient({
      name: 'Dupont',
      firstname: 'François',
      phone: '+33666666666',
      email: 'françois@dupont.fr',
      role: RecipientRole.Signer,
      options: {
        key: 'value'
      }
    })
    .addRecipient({
      name: 'Dupont',
      firstname: 'Claire',
      phone: '+33677777777',
      email: 'claire@dupont.fr',
      role: RecipientRole.Signer,
    })
    .setData({
      name: 'Lud',
    })
    .withMessage('Hello World', 'Subject')
    .withFinalDocMessage(new BasicRequest({templateName: 'superadmincompany-admin-test-small-docx-1'}))
}

function fixtureMainTargets(): Target[] {
  return [{ target: TargetTypes.ZohoCrm, id: '123', type: 'Leads'}]
}

function fixtureAdditionalTarget (): Target{
  return { target: TargetTypes.Gema, id: '451235', type: 'Contacts'}
}

function fixtureDispatcherWithTargets(): ESignDispatcher {
  return fixtureESignDispatcher()
    .addTargets(fixtureMainTargets())
    .addTarget(fixtureAdditionalTarget())
}

const EXPECTED_1 = {
  provider: 'SIGNATURIT',
  subject: 'Subject',
  messageBody: 'Hello World',
  finalDocMessage: {
    templateName: 'superadmincompany-admin-test-small-docx-1'
  },
  recipients: [
    {
      name: 'Dupont',
      firstname: 'François',
      phone: '+33666666666',
      email: 'françois@dupont.fr',
      role: 'SIGNER',
      options: {
        key: 'value'
      }
    },
    {
      name: 'Dupont',
      firstname: 'Claire',
      phone: '+33677777777',
      email: 'claire@dupont.fr',
      role: 'SIGNER',
    },
  ],
  documents: [
    {
      url: 'https://site.com/file.pdf',
    },
    {
      content: 'dlkfzdlgfzed',
    },
    {
      templateName: 'superadmincompany-admin-test-small-docx-1',
      data: {
        name: 'Steve',
      },
    },
  ],
  data: {
    name: 'Lud',
  }
}

describe('Esign service', () => {
  it('should set url, method and payload data', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.method).toBe('post')
      expect(req.url.toString()).toMatch(/\/esign$/u)
      expect(req.payload).toMatchObject(EXPECTED_1)
    })
      .withBaseService(fixtureESignDispatcher())
      .dispatch()
  })

  it('should set the /validate path on validate', () => {
    const payload = EXPECTED_1

    return makeDDStub<ESignDispatcherPayload>(function (req: RequestDescriptor) {
      expect(req.payload).toMatchObject(payload)
      expect(req.url.toString()).toMatch(/\/validate$/u)
    })
      .withBaseService(fixtureESignDispatcher())
      .validateOnly()
      .dispatch()
  })

  it('can have a composed dispatcher', () => {
    const payload = EXPECTED_1
    const targets = fixtureMainTargets()
    targets.push(fixtureAdditionalTarget())

    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.payload).toMatchObject(payload)
      expect(req.url.toString()).toMatch(/\/upload$/u)
      expect(req.payload.targets).toEqual(targets)
    })
      .withBaseService(fixtureDispatcherWithTargets())
      .compose(new UploadDispatcher())
      .dispatch()
  })

  it('should execute a payload validation and succeeds', async () => {
    const response = await makeLiveDD<ESignDispatcherPayload>()
      .withBaseService(fixtureESignDispatcher())
      .validateOnly()
      .dispatch()
    expect(response.status).toBe(200)
    expect(response.data).toBe('')
  })
})
