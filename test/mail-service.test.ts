import { Attachment } from '../src/dispatcher/interfaces/attachments'
import BaseService from '../src/dispatcher/interfaces/BaseService'
import MailDispatcher, { MailDispatcherPayload } from '../src/dispatcher/MailDispatcher'
import RequestDescriptor from '../src/http/interfaces/RequestDescriptor'
import makeLiveDD from './live-tests/dd-live'
import makeDDStub from './mocks/dd-http-stub'

function fixtureMailDispatcher(): MailDispatcher {
  return new MailDispatcher({
    subject: 'Test de mail',
    from: 'someone@somewhere.fr',
    body: 'Hello',
  })
    .setBody('contenu du mail à envoyer')
    .addTo(['somebody@elsewhere.com'])
    .addCc(['somebody@elsewhere.com'])
    .addBcc(['somebody@elsewhere.com'])
    .attach(new Attachment({
      resultFileName: 'fichier',
      templateName: 'superadmincompany-admin-test-mail-1',
    }))
    .setData({
      name: 'Foo',
      breadcrumb: ['first', '2nd', 3],
    })
}

function fixtureMailDispatcherWithTemplatableBody(): MailDispatcher {
  return new MailDispatcher({
    templateName: 'email-template-0',
    data:{
      name: 'Foo',
      breadcrumb: ['first', '2nd', 3],
    }
  })
    .addTo(['somebody@elsewhere.com'])
    .addCc(['somebody@elsewhere.com'])
    .addBcc(['somebody@elsewhere.com'])
    .attach(new Attachment({
      resultFileName: 'fichier',
      templateName: 'superadmincompany-admin-test-mail-1',
    }))
}

const EXPECTED_1 = {
  subject: 'Test de mail',
  from: 'someone@somewhere.fr',
  body: 'contenu du mail à envoyer',
  to: ['somebody@elsewhere.com'],
  attachments: [
    {
      resultFileName: 'fichier',
      templateName: 'superadmincompany-admin-test-mail-1',
    },
  ],
  data: {
    name: 'Foo',
    breadcrumb: ['first', '2nd', 3],
  },
}

const EXPECTED_WITH_BODY = {
  templateName: 'email-template-1',
  to: ['somebody@elsewhere.com'],
  attachments: [
    {
      resultFileName: 'fichier',
      templateName: 'superadmincompany-admin-test-mail-1',
    },
  ],
  data: {
    name: 'Foo',
    breadcrumb: ['first', '2nd', 3],
  },
}

describe('Email service', () => {
  it('should set url, method and payload data', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.method).toBe('post')
      expect(req.headers).toHaveProperty('accept', BaseService.RETURN_FORMATS.PDF)
      expect(req.url.toString()).toMatch(/\/email$/u)
      expect(req.payload).toMatchObject(EXPECTED_1)
    })
      .withBaseService(fixtureMailDispatcher())
      .dispatch()
  })

  it('should set url, method and override payload data for a templatable email', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.method).toBe('post')
      expect(req.headers).toHaveProperty('accept', BaseService.RETURN_FORMATS.PDF)
      expect(req.url.toString()).toMatch(/\/email$/u)
      expect(req.payload).toMatchObject(EXPECTED_WITH_BODY)
    })
      .withBaseService(fixtureMailDispatcher().setTemplateName('email-template-1'))
      .dispatch()
  })

  it('should set url, method and payload data for a templatable email with attachments as DOCx file', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.method).toBe('post')
      expect(req.headers).toHaveProperty('accept', BaseService.RETURN_FORMATS.DOCX)
      expect(req.url.toString()).toMatch(/\/email$/u)
      expect(req.payload).toMatchObject(EXPECTED_WITH_BODY)
    })
      .withBaseService(fixtureMailDispatcherWithTemplatableBody()
        .setTemplateName('email-template-1')
        .setResultFormat(BaseService.RETURN_FORMATS.DOCX))
      .dispatch()
  })

  it('should set the /validate path on validate', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.payload).toMatchObject(EXPECTED_1)
      expect(req.url.toString()).toMatch(/\/validate$/u)
    })
      .withBaseService(fixtureMailDispatcher())
      .validateOnly()
      .dispatch()
  })

  it('should fail validation if root data is missing', () => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    return makeDDStub(function (_req: RequestDescriptor) { })
      .withBaseService(new MailDispatcher({ subject: 'Test de mail', from: 'someone@somewhere.fr', body: 'Hello' })
        .addTo('test@test.com')
        .attach(new Attachment({
          resultFileName: 'fichier',
          templateName: 'superadmincompany-admin-test-mail-1',
          // The data is not present here, and there is no data on the root payload
        })))
      .validate(errors => {
        expect(errors.length === 1).toBeTruthy()
        // discard error
        return true
      })
      .dispatch()
      .then(_ => {
        expect.assertions(1)
      })
  })

  it('should execute a payload validation and succeeds', async () => {
    const response = await makeLiveDD<MailDispatcherPayload>()
      .withBaseService(fixtureMailDispatcher())
      .validateOnly()
      .dispatch()
    expect(response.status).toBe(200)
    expect(response.data).toBe('')
  })

  it('should execute a payload validation and succeeds for a templatable email', async () => {
    const response = await makeLiveDD<MailDispatcherPayload>()
      .withBaseService(fixtureMailDispatcherWithTemplatableBody())
      .validateOnly()
      .dispatch()
    expect(response.status).toBe(200)
    expect(response.data).toBe('')
  })

  it('should execute a payload validation and handle the error', async () => {
    await makeLiveDD<MailDispatcherPayload>()
      .withBaseService(new MailDispatcher({
        subject: 'Test de mail',
        from: 'someone@somewhere.fr',
        body: 'Hello',
      }).addTo('dest@mail.com'))
      .validateOnly()
      .dispatch()
      .then(
        _response => {
          expect(false).toBeTruthy()
        },
        err => {
          expect(err.response.status).toBe(417)
          expect(err.response.statusText).toBe('Expectation Failed')
        }
      )
  })
})
