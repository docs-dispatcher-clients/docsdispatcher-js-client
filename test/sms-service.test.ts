import SMSDispatcher, { SMSDispatcherPayload } from '../src/dispatcher/SMSDispatcher'
import RequestDescriptor from '../src/http/interfaces/RequestDescriptor'
import makeLiveDD from './live-tests/dd-live'
import makeDDStub from './mocks/dd-http-stub'

describe('SMS service', () => {
  it('should set url, method and payload data', () => {
    return makeDDStub<SMSDispatcherPayload>(function (req: RequestDescriptor) {
      expect(req.method).toBe('post')
      expect(req.url.toString()).toMatch(/\/sms$/u)
      expect(req.payload).toMatchObject({
        smsContent: 's€ndinblue çà test',
        to: ['+33600000000'],
        from: 'TOMYEAH',
        provider: 'SENDINBLUE',
      })
    })
      .withBaseService(new SMSDispatcher({
        smsContent: 's€ndinblue çà test',
        to: '+33600000000',
        from: 'TOMYEAH',
        provider: 'SENDINBLUE',
      }))
      .validate()
      .dispatch()
  })

  it('should set the /validate path on validate', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.url.toString()).toMatch(/\/validate$/u)
    })
      .withBaseService(new SMSDispatcher({
        smsContent: 's€ndinblue çà test',
        to: '+33600000000',
        from: 'TOMYEAH',
        provider: 'SENDINBLUE',
      }))
      .validateOnly()
      .dispatch()
  })

  it('should execute a payload validation and succeeds', async () => {
    const response = await makeLiveDD<SMSDispatcherPayload>()
      .withBaseService(new SMSDispatcher({
        smsContent: 's€ndinblue çà test',
        to: '+33600000000',
        from: 'TOMYEAH',
        provider: 'SENDINBLUE',
      }))
      .validateOnly()
      .dispatch()
    expect(response.status).toBe(200)
    expect(response.data).toBe('')
  })
})
