import BasicAuth from '../../src/auth/BasicAuth'
import { Payload } from '../../src/dispatcher/interfaces/generics'
import HttpResponse from '../../src/http/interfaces/HttpResponse'
import RequestDescriptor from '../../src/http/interfaces/RequestDescriptor'
import DocsDispatcher from '../../src/main'
import HttpMock from './HttpMock'

/**
 *
 * Creates a DocsDispatcher instance with basic auth. The http layer will be
 * handled by the given callback
 * @param httpCallback The Http handler
 * @param forceResponse If true, the http layer will return a 200 response if
 *                      the callback returns void. Defaults to true
 */
export default function makeDDStub<P extends Payload>(
  httpCallback: (req: RequestDescriptor) => Promise<HttpResponse> | void,
  forceResponse = true
): DocsDispatcher<P> {
  const auth = new BasicAuth('dummy', 'dummy')
  const http = new HttpMock(function(req: RequestDescriptor): Promise<HttpResponse> {
    const result = httpCallback(req)
    if (forceResponse && void 0 === result) {
      return Promise.resolve({ status: 200, data: '', headers: {} })
    }
    if (void 0 === result) {
      throw new Error('Expected an HttpResponse to be returned')
    }
    return result as Promise<HttpResponse>
  })

  return new DocsDispatcher(auth, http)
}
