import { AxiosResponseHeaders, RawAxiosResponseHeaders } from 'axios'

/**
 * Custom light http response type.
 */
export default interface HttpResponse {
  status: number;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: Record<string, any> | string | Blob;
  headers: RawAxiosResponseHeaders | AxiosResponseHeaders;
}