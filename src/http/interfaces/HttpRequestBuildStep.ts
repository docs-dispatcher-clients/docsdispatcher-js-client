import HttpRequestBuilder from '../HttpRequestBuilder'

/**
 * All request descriptor modifications are mutations. The transform method
 * of a step must mutate the request descriptor.
 */
export default interface HttpRequestBuildStep {
  transformRequest(builder: HttpRequestBuilder): void;
}