import { AxiosInstance, AxiosRequestConfig, AxiosResponse, ResponseType as AxiosResponseType } from 'axios'
import HttpAdapter from '../interfaces/HttpAdapter'
import HttpResponse from '../interfaces/HttpResponse'
import RequestDescriptor, { ResponseType } from '../interfaces/RequestDescriptor'

function castToResponse (axiosResponse: AxiosResponse): HttpResponse {
  return axiosResponse
}

export default class AxiosAdapter implements HttpAdapter {
  private _axios: AxiosInstance

  public constructor(axios: AxiosInstance) {
    this._axios = axios
  }

  public async send (req: RequestDescriptor, isValidate: boolean): Promise<HttpResponse> {
    const responseType: AxiosResponseType | undefined = this.getAxiosResponseType(isValidate, req.responseType)

    const url = req.url.toString()
    const { payload } = req
    const axiosData: AxiosRequestConfig = Object.assign({}, req, { url, responseType })
    axiosData.data = payload
    delete (axiosData as Record<string, unknown>).payload
    const axiosResponse = await this._axios(axiosData)
    return castToResponse(axiosResponse)
  }

  private getAxiosResponseType (isValidate: boolean, rt?: ResponseType): AxiosResponseType | undefined {
    if (isValidate) {
      return void 0
    }
    return rt
  }
}
