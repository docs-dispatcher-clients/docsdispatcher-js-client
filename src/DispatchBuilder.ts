import BaseService, { SERVICE_TYPE } from './dispatcher/interfaces/BaseService'
import ComposedService from './dispatcher/interfaces/ComposedService'
import { Payload } from './dispatcher/interfaces/generics'
import HRB from './http/HttpRequestBuilder'
import HttpRequestBuildStep from './http/interfaces/HttpRequestBuildStep'
import DocsDispatcher from './main'
import { ValidationError } from './validation/interfaces'

type RequestTransformer = (req: HRB) => void
type ValidationErrorHandler = (errors: ValidationError[]) => boolean

export default class DispatchBuilder<P extends Payload> implements HttpRequestBuildStep {

  private baseService: BaseService<P>

  private composedServices: ComposedService[] = []

  private parent: DocsDispatcher<P>

  private isValidation = false

  /**
   * Transforms for the http request builders that will be applied after the
   * front and composed services are applied
   */
  private endTransforms: Array<RequestTransformer> = []

  public constructor(parent: DocsDispatcher<P>, baseService: BaseService<P>) {
    this.parent = parent
    this.baseService = baseService
  }

  public isValidateOnly (): boolean {
    return this.isValidation
  }

  public compose (service: ComposedService): this {
    this.composedServices.push(service)
    return this
  }

  public dispatch (): ReturnType<DocsDispatcher<P>['dispatch']> {
    return this.parent.dispatch(this)
  }

  public toRequest (): ReturnType<DocsDispatcher<P>['toRequest']> {
    return this.parent.toRequest(this)
  }

  public validateOnly (): this {
    if (!this.isValidation) {
      this.addEndTransform((req: HRB) => req.addPathSegment('/validate'))
      this.isValidation = true
    } else {
      console.warn('The validate() flag has already been set.')
    }
    return this
  }

  public transformRequest (req: HRB): void {
    req
      .call(b => this.buildFront(b))
      .call(b => this.buildComposed(b))
      .call(b => DispatchBuilder.applyTransforms(b, this.endTransforms))
  }

  public validate (errorHandler?: ValidationErrorHandler): this {
    const errors: ValidationError[] = []
    this.baseService.validate(errors)
    this.composedServices.forEach(cs => cs.validate(errors))
    console.error(errors)
    if (errors.length) {
      let ignoreErrors = false
      if (errorHandler) {
        ignoreErrors = errorHandler(errors)
      }
      if (!ignoreErrors) {
        throw new Error(`Errors during validation: ${JSON.stringify(errors, void 0, 2)}`)
      }
    }
    return this
  }

  private addEndTransform (fn: RequestTransformer): void {
    this.endTransforms.push(fn)
  }

  private buildFront (req: HRB): void {
    req
      .setPath(this.baseService.getBasePath())
      .addAcceptedType(this.getReturnType())
      .setPayload(this.baseService.asPayload())
    if (this.baseService.getServiceType() === SERVICE_TYPE.FILE) {
      req.setResponseType('stream')
    }
  }

  private getReturnType (): string {
    return this.baseService.getResultFormat()
  }

  private buildComposed (b: HRB): void {
    this.composedServices.reduce(
      (hrb: HRB, composed) => hrb.addPathSegment(composed.getPathSegment())
      , b
    )
  }

  private static applyTransforms (req: HRB, transforms: Array<RequestTransformer>): void {
    transforms.reduce((b: HRB, fn: RequestTransformer) => {
      fn(b)
      return b
    }, req)
  }
}