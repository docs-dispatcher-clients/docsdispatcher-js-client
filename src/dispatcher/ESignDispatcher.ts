import { applyMixins } from '../util/apply-mixins'
import { ValidationError } from '../validation/interfaces'
import BaseService, { SERVICE_TYPE } from './interfaces/BaseService'
import { Attachment, AttachmentCollectionPayload } from './interfaces/attachments'
import { BasicRequest } from './interfaces/basic-requests'
import { MaybeData, TargetablePayload, WithSettingsPayload } from './interfaces/generics'
import TargetableMixin from './mixins/TargetableMixin'
import WithSettingsMixin from './mixins/WithSettingsMixin'

export interface Recipient {
  id?: string;
  name?: string;
  firstname?: string;
  phone?: string;
  email?: string;
  role: RecipientRole;
  targetUsers?: string[];
  options?: MaybeData;
}

export interface ESignField {
  recipient: string;
  anchorKey?: string;
  type?: FieldType;
  additionalOptions?: string;
  consents?: string[];
}

export enum FieldType {
  Signature = 'SIGNATURE',
  Visa = 'VISA'
}

export enum RecipientRole {
  Signer = 'SIGNER',
  Approver = 'APPROVER',
  Observer = 'OBSERVER'
}

export enum DeliveryTypes {
  Email = 'EMAIL',
  Url = 'URL',
  Sms='SMS',
  Web ='WEB'
}

export enum SignatureTypes {
  Simple = 'SIMPLE',
  Certified = 'CERTIFIED',
  Smart = 'SMART',
  Advanced = 'ADVANCED'
}

export enum SigningModes {
  Sequential = 'SEQUENTIAL',
  Parallel = 'PARALLEL'
}

export interface ESignDispatcherPayload extends WithSettingsPayload, TargetablePayload {
  provider?: string;

  subject?: string;
  messageBody?: string;
  message?: BasicRequest;
  recipients: Recipient[];
  documents: AttachmentCollectionPayload;
  data?: MaybeData;

  finalDocMessageSubject?: string;
  finalDocMessageBody?: string;
  finalDocMessage?: BasicRequest;
  finalDocRecipients?: Recipient[];

  type?: SignatureTypes;
  deliveryType?: DeliveryTypes;
  signingMode?: SigningModes;
  expirationTime?: number;

  fields?: ESignField[]
}

class ESignDispatcher extends BaseService<ESignDispatcherPayload> {

  protected provider?: string

  protected subject?: string

  protected messageBody?: string

  protected message?: BasicRequest

  protected recipients: Recipient[] = []

  protected documents: Attachment[] = []

  protected data?: MaybeData

  protected finalDocMessageSubject?: string

  protected finalDocMessageBody?: string

  protected finalDocMessage?: BasicRequest

  protected finalDocRecipients?: Recipient[] = []

  // signature configuration shared among all providers
  protected type?: SignatureTypes

  protected deliveryType?: DeliveryTypes

  protected signingMode?: SigningModes

  protected expirationTime?: number

  protected fields?: ESignField[]

  private basePath = '/api/esign'

  public getAcceptedMimetypes (): string[] {
    return [BaseService.RETURN_FORMATS.JSON]
  }

  public getServiceType (): SERVICE_TYPE {
    return SERVICE_TYPE.E_SIGN
  }

  public getDefaultFormat (): string {
    return BaseService.RETURN_FORMATS.JSON
  }

  public getBasePath (): string {
    return this.basePath
  }

  public asPayload (): ESignDispatcherPayload {
    return {
      ...this.asTargetablePayload(),
      ...this.asSettingsPayload(),
      ...(this.provider && { provider: this.provider }),
      ...(this.message && { message: this.message }),
      ...(this.subject && { subject: this.subject }),
      ...(this.messageBody && { messageBody: this.messageBody }),
      recipients: this.recipients,
      documents: this.documents.map(x => x.asPayload()),
      ...(this.data && { data: this.data }),
      ...(this.finalDocMessageSubject && { finalDocMessageSubject: this.finalDocMessageSubject }),
      ...(this.finalDocMessageBody && { finalDocMessageBody: this.finalDocMessageBody }),
      ...(this.finalDocMessage && { finalDocMessage: this.finalDocMessage }),
      ...(this.finalDocRecipients && { finalDocRecipients: this.finalDocRecipients }),
      ...(this.type && { type: this.type }),
      ...(this.deliveryType && { deliveryType: this.deliveryType }),
      ...(this.signingMode && { signingMode: this.signingMode }),
      ...(this.expirationTime && { expirationTime: this.expirationTime }),
      ...(this.fields && { fields: this.fields })
    }
  }

  public withProvider (provider: string): this {
    this.provider = provider
    return this
  }

  public withMessage (message: BasicRequest|string, subject?: string): this {
    if (typeof message === 'string'){
      this.messageBody = message
      this.subject = subject
    } else {
      this.message = message
    }
    return this
  }

  public withFinalDocMessage (message: BasicRequest | string, subject?: string): this {
    if (typeof message === 'string'){
      this.finalDocMessageBody = message
      this.finalDocMessageSubject = subject
    } else {
      this.finalDocMessage = message
    }
    return this
  }

  public addDocument (document: Attachment): this {
    this.documents.push(document)
    return this
  }

  public setData (data: MaybeData): this {
    this.data = data
    return this
  }

  public addRecipient (recipient: Recipient): this {
    this.recipients.push(recipient)
    return this
  }

  public setFinalDocRecipients(recipients: Recipient[]): this {
    this.finalDocRecipients = recipients
    return this
  }

  public addFinalDocRecipient (recipient: Recipient): this {
    if (!this.finalDocRecipients){
      this.finalDocRecipients = []
    }
    this.finalDocRecipients.push(recipient)
    return this
  }

  public withSignatureType (type: SignatureTypes): this {
    this.type = type
    return this
  }

  public withDeliveryType (deliveryType: DeliveryTypes): this {
    this.deliveryType = deliveryType
    return this
  }

  public withSigningMode (signingMode: SigningModes): this {
    this.signingMode = signingMode
    return this
  }

  public withExpirationTime (time: number): this {
    this.expirationTime = time
    return this
  }

  public withFields (fields: ESignField[]): this {
    this.fields = fields
    return this
  }

  public addField (field: ESignField): this {
    if (!this.fields) {
      this.fields = []
    }
    this.fields.push(field)
    return this
  }

  public validate (_: ValidationError[]): boolean {
    throw new Error('Method not implemented')
  }

}

applyMixins(ESignDispatcher, [WithSettingsMixin, TargetableMixin])
interface ESignDispatcher extends WithSettingsMixin, TargetableMixin { }

export default ESignDispatcher