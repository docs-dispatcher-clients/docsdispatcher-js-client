import { applyMixins } from '../util/apply-mixins'
import { ValidationError } from '../validation/interfaces'
import BaseService, { SERVICE_TYPE } from './interfaces/BaseService'
import { Attachment, AttachmentCollectionPayload } from './interfaces/attachments'
import { AbstractData, MaybeData, TargetablePayload, WithSettingsPayload } from './interfaces/generics'
import TargetableMixin from './mixins/TargetableMixin'
import WithSettingsMixin from './mixins/WithSettingsMixin'

export enum EnvelopeFormats{
  C4 = 'C4',
  C5 = 'C5',
  C6 = 'C6',
  Auto = 'AUTO'
}

export enum ColorModes{
  Color = 'COLOR',
  BlackAndWhite = 'BW'
}

export enum PostageTypes {
  Eco='ECO',
  RegisteredLetter ='REGISTERED_LETTER',
  RegisteredLetterWithAck = 'REGISTERED_LETTER_WITH_ACK',
  Fast='FAST',
  FastTracked ='FAST_TRACKED',
  Slow='SLOW',
  SlowTracked='SLOW_TRACKED'
}

export interface PostalAddress {
  name: string;
  address1: string;
  address2?: string;
  address3?: string;
  address4?: string;
  city: string;
  zipCode: string;
  countryCode: string;
}

export interface PostalDispatcherProps {
  provider?: string;
  sender: PostalAddress;
  subject?: string;
  // Postal options
  envelopeFormat?: EnvelopeFormats;
  colorMode?: ColorModes;
  postage?: PostageTypes;
  bothSides?: boolean;
  targetFilename?: string;
}

export interface PostalDispatcherPayload extends PostalDispatcherProps, WithSettingsPayload, TargetablePayload {
  receivers: PostalAddress[];
  documents: AttachmentCollectionPayload;
  data?: AbstractData;
}

class PostalDispatcher extends BaseService<PostalDispatcherPayload> {
  protected provider?: string

  protected subject?: string

  protected sender: PostalAddress

  protected receivers: PostalAddress[] = []

  protected documents: Attachment[] = []

  protected data?: AbstractData

  protected envelopeFormat?: EnvelopeFormats

  protected colorMode?: ColorModes

  protected postage?: PostageTypes

  protected bothSides?: boolean

  protected targetFilename?: string

  private basePath = '/api/postal'

  public constructor(props: PostalDispatcherProps) {
    super()
    this.provider = props.provider
    this.sender = props.sender
    this.subject = props.subject
    this.envelopeFormat = props.envelopeFormat
    this.colorMode = props.colorMode
    this.postage = props.postage
    this.bothSides = props.bothSides
    this.targetFilename = props.targetFilename
  }

  public getAcceptedMimetypes (): string[] {
    return [BaseService.RETURN_FORMATS.JSON]
  }

  public getServiceType (): SERVICE_TYPE {
    return SERVICE_TYPE.POSTAL
  }

  public getDefaultFormat (): string {
    return BaseService.RETURN_FORMATS.JSON
  }

  public getBasePath (): string {
    return this.basePath
  }

  public asPayload (): PostalDispatcherPayload {
    return {
      ...this.asTargetablePayload(),
      ...this.asSettingsPayload(),
      ...(this.provider && { provider: this.provider }),
      ...(this.subject && { subject: this.subject }),
      sender: this.sender,
      receivers: this.receivers,
      documents: this.documents.map(x => x.asPayload()),
      ...(this.data && { data: this.data }),
      // Postal options
      ...(this.envelopeFormat && { envelopeFormat: this.envelopeFormat }),
      ...(this.colorMode && { colorMode: this.colorMode }),
      ...(this.postage && { postage: this.postage }),
      ...(this.bothSides && { bothSides: this.bothSides }),
      ...(this.targetFilename && { targetFilename: this.targetFilename })
    }
  }

  public withProvider (provider: string): this {
    this.provider = provider
    return this
  }

  public withSubject (subject: string): this {
    this.subject = subject
    return this
  }

  public withSender (sender: PostalAddress): this {
    this.sender = sender
    return this
  }

  public addReceiver (receiver: PostalAddress): this {
    this.receivers.push(receiver)
    return this
  }

  public addDocument (document: Attachment): this {
    this.documents.push(document)
    return this
  }

  public setData (data: MaybeData): this {
    this.data = data
    return this
  }

  public withEnvelopeFormat (envelopeFormat: EnvelopeFormats): this {
    this.envelopeFormat = envelopeFormat
    return this
  }

  public withColorMode (colorMode: ColorModes): this {
    this.colorMode = colorMode
    return this
  }

  public withPostage (postage: PostageTypes): this {
    this.postage = postage
    return this
  }

  public withBothSides (): this {
    this.bothSides = true
    return this
  }

  public withoutBothSides (): this {
    this.bothSides = false
    return this
  }

  public withTargetFilename (targetFilename: string): this {
    this.targetFilename = targetFilename
    return this
  }

  public validate (_: ValidationError[]): boolean {
    throw new Error('Method not implemented')
  }

}

applyMixins(PostalDispatcher, [WithSettingsMixin, TargetableMixin])
interface PostalDispatcher extends WithSettingsMixin, TargetableMixin { }

export default PostalDispatcher