import { applyMixins } from '../util/apply-mixins'
import { ValidationError } from '../validation/interfaces'
import BaseService, { SERVICE_TYPE } from './interfaces/BaseService'
import { MaybeData, TemplatePayload } from './interfaces/generics'
import TemplatableMixin from './mixins/TemplatableMixin'

export interface SMSDispatcherPayload extends SMSDispatcherProps, TemplatePayload {
  to: string[];
}

export interface SMSDispatcherProps {
  provider?: SMSProvider;
  from?: string;
  to: string|string[];
  smsContent?: string;
  templateName?: string;
  data?: MaybeData;
}

export type SMSProvider = 'OVH' | 'MAILJET' | 'SENDINBLUE' | 'SMS_FACTOR' | 'SMS_MAGIC'

class SMSDispatcher extends BaseService<SMSDispatcherPayload>{

  private basePath = '/api/sms'

  private provider?: SMSProvider

  private from?: string

  private to: string[]

  private smsContent?: string

  public constructor(props: SMSDispatcherProps) {
    super()
    this.provider = props.provider
    this.from = props.from
    this.to = Array.isArray(props.to) ? props.to : [props.to]
    this.smsContent = props.smsContent
    this.templateName = props.templateName
    this.data = props.data
  }

  public getServiceType (): SERVICE_TYPE {
    return SERVICE_TYPE.SMS
  }

  public validate (_errors: ValidationError[]): boolean {
    return true
  }

  public getBasePath (): string {
    return this.basePath
  }

  public asPayload (): SMSDispatcherPayload {
    return {
      ...this.asTemplatePayload(),
      ...(this.provider && { provider: this.provider }),
      ...(this.from && { from: this.from }),
      to: this.to,
      ...(this.smsContent && { smsContent: this.smsContent })
    }
  }

  public getAcceptedMimetypes (): string[] {
    return [BaseService.RETURN_FORMATS.JSON]
  }

  public getDefaultFormat (): string {
    return BaseService.RETURN_FORMATS.JSON
  }

}

applyMixins(SMSDispatcher, [TemplatableMixin])
interface SMSDispatcher extends TemplatableMixin { }

export default SMSDispatcher