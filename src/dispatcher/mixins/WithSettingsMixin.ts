import { MaybeData, WithSettings, WithSettingsPayload } from '../interfaces/generics'

export type WithSettingsPreset = { settings?: MaybeData }

export default class WithSettingsMixin implements WithSettings {

  protected settings?: MaybeData

  public addSetting (key: string, value: unknown): this {
    this.settings = Object.assign(this.settings || {}, { [key]: value })
    return this
  }

  public setSettings (settings: Record<string, unknown>): this {
    this.settings = settings
    return this
  }

  public asSettingsPayload(): WithSettingsPayload {
    return {
      settings: this.settings
    }
  }

  protected setSettingsPreset(preset: WithSettingsPreset): void {
    const { settings } = preset
    Object.assign(this, { settings })
  }
}