import { FileContentRequest, FileContentRequestPayload } from '../interfaces/attachments'
import TemplatableMixin, { TemplatePreset } from './TemplatableMixin'

export type FileContentPreset = TemplatePreset & { content?: string; url?: string }

export default class FileContentMixin extends TemplatableMixin implements FileContentRequest {

  protected content?: string

  protected url?: string

  public setContent (content: string): this {
    this.content = content
    return this
  }

  public setUrl (url: string): this {
    this.url = url
    return this
  }

  public asFileContentPayload (): FileContentRequestPayload {
    return {
      ...super.asTemplatePayload(),
      content: this.content,
      url: this.url
    }
  }

  protected setFileContentPreset (preset: FileContentPreset): void {
    super.setTemplatePreset(preset)
    const { content, url } = preset
    Object.assign(this, { content, url })
  }
}