import { applyMixins } from '../../util/apply-mixins'
import FileContentMixin, { FileContentPreset } from '../mixins/FileContentMixin'
import TargetableMixin, { TargetablePreset } from '../mixins/TargetableMixin'
import TemplatableMixin from '../mixins/TemplatableMixin'
import { PayloadGenerator, TargetablePayload, Templatable, TemplatePayload } from './generics'

export type FileContentRequestPayload = TemplatePayload & {
  content?: string;
  url?: string;
}

export interface FileContentRequest extends Templatable {
  setContent(content: string): this;
  setUrl(url: string): this;
}

export type AttachmentPayload = FileContentRequestPayload & TargetablePayload

export type AttachmentCollectionPayload = AttachmentPayload[]

export class Attachment implements PayloadGenerator<AttachmentPayload> {
  public constructor(preset: FileContentPreset & TargetablePreset) {
    if (preset) {
      this.setFileContentPreset(preset)
      this.setTargetablePreset(preset)
    }
  }

  public asPayload(): AttachmentPayload {
    return { ...this.asFileContentPayload(), ...this.asTargetablePayload() }
  }
}

applyMixins(Attachment, [TemplatableMixin, FileContentMixin, TargetableMixin])
export interface Attachment extends TemplatableMixin, FileContentMixin, TargetableMixin {}

export type AttachmentCollection = Attachment[]
