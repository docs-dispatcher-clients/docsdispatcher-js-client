import { ValidationError } from '../../validation/interfaces'
import { PayloadGenerator } from './generics'

export enum SERVICE_TYPE {
  FILE,
  MAIL,
  POSTAL,
  SMS,
  E_SIGN
}

export default abstract class BaseService<P> implements PayloadGenerator<P> {
  public static RETURN_FORMATS: { [index: string]: string } = {
    PDF: 'application/pdf',
    HTML: 'text/html',
    TEXT: 'text/plain',
    XLSX: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    DOCX: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    PPTX: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    SVG: 'image/svg+xml',
    PNG: 'image/png',
    JPEG: 'image/jpeg',
    JSON: 'application/json'
  }

  private resultFormat?: string

  public setResultFormat (resultFormat: string): this {
    resultFormat = resultFormat || this.getDefaultFormat()
    if (!this.getAcceptedMimetypes().includes(resultFormat)) {
      throw new Error('Value \'' + resultFormat + '\' is invalid for this service. Accepted values: ' + this.getAcceptedMimetypes())
    }
    this.resultFormat = resultFormat
    return this
  }

  public getResultFormat (): string {
    return this.resultFormat || this.getDefaultFormat()
  }

  public abstract asPayload (): P;

  public abstract getBasePath (): string;

  public abstract getDefaultFormat (): string;

  public abstract getServiceType (): SERVICE_TYPE;

  public abstract getAcceptedMimetypes (): string[];

  public abstract validate (errors: ValidationError[]): boolean;
}