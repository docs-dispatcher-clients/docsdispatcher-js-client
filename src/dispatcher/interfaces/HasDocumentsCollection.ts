import { Attachment, AttachmentCollection } from './attachments'

import { ValidationError } from '../../validation/interfaces'
import { MaybeData } from './generics'

export type CollectionKeys = string[]

export default interface HasDocumentCollections {
  getAttachmentCollectionsKeys(): CollectionKeys;
  getAttachmentCollection(key: string): AttachmentCollection;
  getData(): MaybeData;
}

export function validateDocumentCollections(service: HasDocumentCollections, errors: ValidationError[]): boolean {
  const hasRootData = typeof service.getData() !== 'undefined'
  let valid = true
  if (!hasRootData) {
    service.getAttachmentCollectionsKeys().forEach(key => {
      const docs = service.getAttachmentCollection(key)
      docs.forEach((doc: Attachment, i) => {
        if (typeof doc.getData() === 'undefined' || doc.getData() === null) {
          valid = false
          errors.push({path: [key, i], message: 'Missing document data'})
        }
      })
    })
  }
  return valid
}
