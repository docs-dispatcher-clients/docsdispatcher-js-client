import axios from 'axios'
import DispatchBuilder from './DispatchBuilder'
import BasicAuth from './auth/BasicAuth'
import AuthAdapter from './auth/interfaces/AuthAdapter'
import ESignDispatcher from './dispatcher/ESignDispatcher'
import FileDispatcher from './dispatcher/FileDispatcher'
import MailDispatcher from './dispatcher/MailDispatcher'
import PostalDispatcher from './dispatcher/PostalDispatcher'
import SMSDispatcher from './dispatcher/SMSDispatcher'
import UploadDispatcher from './dispatcher/UploadDispatcher'
import BaseService from './dispatcher/interfaces/BaseService'
import { Attachment } from './dispatcher/interfaces/attachments'
import { Payload } from './dispatcher/interfaces/generics'
import HRB from './http/HttpRequestBuilder'
import AxiosAdapter from './http/adapters/AxiosAdapter'
import HttpAdapter from './http/interfaces/HttpAdapter'
import HttpResponse from './http/interfaces/HttpResponse'
import RequestDescriptor from './http/interfaces/RequestDescriptor'

const DEFAULT_OPTS = {
  endpoint: 'https://api.docs-dispatcher.io'
}

type CreateOpts = {
  username: string;
  password: string;
  endpoint?: string;
}

export interface DocsDispatcherOpts {
  endpoint?: string;
}

export default class DocsDispatcher<P extends Payload> {

  // Export every part of the library into an accessible namespace
  public static Http = { AxiosAdapter }

  public static Service = {
    FileDispatcher,
    SMSDispatcher,
    MailDispatcher,
    PostalDispatcher,
    ESignDispatcher,
  }

  public static EmailAttachment = Attachment

  public static ComposedService = {
    UploadDispatcher
  }

  public static RETURN_FORMATS = BaseService.RETURN_FORMATS

  public static Auth = {
    BasicAuth
  }

  private auth: AuthAdapter

  private http: HttpAdapter

  private opts: DocsDispatcherOpts = DEFAULT_OPTS

  public constructor(auth: BasicAuth, http: HttpAdapter, opts?: DocsDispatcherOpts) {
    this.auth = auth
    this.http = http
    this.mergeOpts(opts || {})
  }

  public mergeOpts (opts: DocsDispatcherOpts): void {
    this.opts = Object.assign({}, this.opts, opts)
  }

  public healthCheck (): Promise<HttpResponse> {
    const req = HRB.from({
      url: this.opts.endpoint + '/healthz',
      method: 'get'
    })
      .pipe(this.auth)
      .make()

    // @todo response parser
    return this.http.send(req, false)
  }

  // @todo find a better name
  public withBaseService (service: BaseService<P>): DispatchBuilder<P> {
    return new DispatchBuilder<P>(this, service)
  }

  public dispatch (dispatchBuilder: DispatchBuilder<P>): ReturnType<HttpAdapter['send']> {
    // @todo response parser
    return this.http.send(this.toRequest(dispatchBuilder), dispatchBuilder.isValidateOnly())
  }

  public toRequest (dispatchBuilder: DispatchBuilder<P>): RequestDescriptor {
    return HRB.from({
      url: this.opts.endpoint,
      method: 'post'
    })
      .pipe(this.auth)
      .pipe(dispatchBuilder)
      .make()
  }

  public static create<U extends Payload> (opts: CreateOpts): DocsDispatcher<U> {

    const auth = new BasicAuth(opts.username, opts.password)
    const http = new AxiosAdapter(axios)

    const { endpoint } = opts
    const ddOpts = { endpoint }

    return new DocsDispatcher<U>(auth, http, ddOpts)
  }
}
